#!/usr/bin/env python

from dataclasses import dataclass
from enum import Enum, auto
from itertools import chain


class Doors(Enum):
    LEFT = auto()
    RIGHT = auto()
    BOTH = auto()

    def entering(self):
        if self == self.LEFT:
            part = "the left"
        elif self == self.RIGHT:
            part = "the right"
        elif self == self.BOTH:
            part = "both sides"
        return f"Doors will open on {part}."

    __str__ = entering


@dataclass
class Station:
    name: str
    changes: str = ""
    no_smoking: bool = False

    def entering(self):
        parts = []
        if self.changes:
            parts.append(f"Change here for the {self.changes}.")
        if self.no_smoking:
            parts.append("No smoking, please.")
        if parts:
            parts.append("")
        return " ".join(parts)

    def __str__(self):
        return self.name


@dataclass
class Platform:
    station: Station
    doors: Doors

    def entering(self):
        return f"Entering: {self.station}. {self.station.entering()}{self.doors}"

    @classmethod
    def Right(cls, *args, **kwargs):
        return cls(Station(*args, **kwargs), Doors.RIGHT)

    @classmethod
    def Left(cls, *args, **kwargs):
        return cls(Station(*args, **kwargs), Doors.LEFT)


@dataclass
class LineRun:
    platforms: list[Platform]

    @property
    def destination(self):
        return self.platforms[-1]

    def announce_destination(self):
        return f"The destination of this train is: {self.destination.station}."

    def ride_line(self):
        yield self.announce_destination()
        for next_stop in self.platforms[1:]:
            yield f"Next Stop: {next_stop.station}."
            yield next_stop.entering()
            if next_stop is self.destination:
                dest = "Last stop. Thank you for taking the T. Don't forget your belongings."
            else:
                dest = self.announce_destination()
            yield f"{next_stop.station}. {dest}"

    __iter__ = ride_line


NORTH_STATION = Station("North Station", "Orange Line and Commuter Rail")
HAYMARKET = Station("Haymarket", "Orange Line")
GOVERNMENT_CENTER = Station("Government Center", "Blue Line")
PARK_STREET = Station("Park Street", "Red and Orange Lines")

HYNES_PLAT = Platform.Right("Hynes Convention Center")

BOYLSTON_INBOUND = [
    Platform.Right("Copley"),
    Platform.Right("Arlington"),
    Platform.Right("Boylston", no_smoking=True),
]

BOYLSTON_OUTBOUND = list(reversed(BOYLSTON_INBOUND))

CE_SUBWAY_OUTBOUND = [
    Platform(NORTH_STATION, Doors.LEFT),
    Platform(HAYMARKET, Doors.LEFT),
    Platform(GOVERNMENT_CENTER, Doors.LEFT),
    Platform(PARK_STREET, Doors.LEFT),
    *BOYLSTON_OUTBOUND,
]

CE_SUBWAY_INBOUND = [
    *BOYLSTON_INBOUND,
    Platform(PARK_STREET, Doors.BOTH),
    Platform(GOVERNMENT_CENTER, Doors.LEFT),
    Platform(HAYMARKET, Doors.LEFT),
    Platform(NORTH_STATION, Doors.RIGHT),
]

E_SUBWAY_OUTBOND = [
    Platform.Right("Lechmere"),
    Platform.Right("Science Park"),
    *CE_SUBWAY_OUTBOUND,
    Platform.Right("Prudential"),
    Platform.Right("Symphony"),
]

E_SUBWAY_INBOUND = [
    Platform.Right("Symphony"),
    Platform.Right("Prudential"),
    *CE_SUBWAY_INBOUND,
    Platform.Right("Science Park"),
    Platform.Right("Lechmere"),
]

D_SUBWAY_OUTBOUND = [
    Platform(GOVERNMENT_CENTER, Doors.LEFT),
    Platform(PARK_STREET, Doors.RIGHT),
    *BOYLSTON_OUTBOUND,
    HYNES_PLAT,
    Platform.Left("Kenmore"),
]

D_SUBWAY_INBOUND = [
    Platform.Left("Kenmore"),
    HYNES_PLAT,
    *BOYLSTON_INBOUND,
    Platform(PARK_STREET, Doors.BOTH),
    Platform(GOVERNMENT_CENTER, Doors.LEFT),
]

C_SUBWAY_OUTBOUND = [
    *CE_SUBWAY_OUTBOUND,
    HYNES_PLAT,
    Platform.Left("Kenmore"),
]

C_SUBWAY_INBOUND = [
    Platform.Left("Kenmore"),
    HYNES_PLAT,
    *CE_SUBWAY_INBOUND,
]

B_SUBWAY_OUTBOUND = [
    Platform(PARK_STREET, Doors.RIGHT),
    *BOYLSTON_OUTBOUND,
    HYNES_PLAT,
    Platform.Right("Kenmore"),
]

# doors are all the same inbound and outbound on B subway!
B_SUBWAY_INBOUND = list(reversed(B_SUBWAY_OUTBOUND))

E_SURFACE = [
    Platform.Right("Heath Street/VA Medical Center"),
    Platform.Right("Back of the Hill"),
    Platform.Right("Riverway"),
    Platform.Right("Mission Park"),
    Platform.Right("Fenwood Road"),
    Platform.Right("Brigham Circle"),
    Platform.Right("Longwood Medical Area"),
    Platform.Right("Museum of Fine Arts"),
    Platform.Right("Northeastern University", no_smoking=True),
]

D_SURFACE = [
    Platform.Right("Riverside"),
    Platform.Right("Woodland"),
    Platform.Right("Waban"),
    Platform.Right("Eliot"),
    Platform.Right("Newton Highlands"),
    Platform.Right("Newton Center"),
    Platform.Right("Chestnut Hill"),
    Platform.Right("Reservoir", no_smoking=True),
    Platform.Right("Beaconsfield"),
    Platform.Right("Brookline Hills"),
    Platform.Right("Brookline Village"),
    Platform.Right("Longwood"),
    Platform.Right("Fenway"),
]

C_SURFACE = [
    Platform.Right("Cleveland Circle"),
    Platform.Right("Englewood Avenue"),
    Platform.Right("Dean Road"),
    Platform.Right("Tappan Street"),
    Platform.Right("Washington Square"),
    Platform.Right("Fairbanks Street"),
    Platform.Right("Brandon Hall"),
    Platform.Right("Winchester Street/Summit Avenue"),
    Platform.Right("Coolidge Corner", no_smoking=True),
    Platform.Right("St. Paul Street"),
    Platform.Right("Kent Street"),
    Platform.Right("Hawes Street"),
    Platform.Right("St. Marys Street"),
]

B_SURFACE = [
    Platform.Right("Boston College"),
    Platform.Right("South Street"),
    Platform.Right("Chestnut Hill Avenue"),
    Platform.Right("Chiswick Road"),
    Platform.Right("Sutherland Road"),
    Platform.Right("Washington Street"),
    Platform.Right("Warren Street"),
    Platform.Right("Allston Street"),
    Platform.Right("Griggs Street/Long Avenue"),
    Platform.Right("Harvard Avenue", no_smoking=True),
    Platform.Right("Brighton Avenue/Packards Corner"),
    Platform.Right("Babcock Street"),
    Platform.Right("Pleasant Street"),
    Platform.Right("St. Paul Street"),
    Platform.Right("Boston University West"),
    Platform.Right("Boston University Central"),
    Platform.Right("Boston University East"),
    Platform.Right("Blandford Street"),
]

E_INBOUND = [*E_SURFACE, *E_SUBWAY_INBOUND]

E_OUTBOUND = [*E_SUBWAY_OUTBOND, *reversed(E_SURFACE)]

D_INBOUND = [*D_SURFACE, *D_SUBWAY_INBOUND]

D_OUTBOUND = [*D_SUBWAY_OUTBOUND, *reversed(D_SURFACE)]

C_INBOUND = [*C_SURFACE, *C_SUBWAY_INBOUND]

C_OUTBOUND = [*C_SUBWAY_OUTBOUND, *reversed(C_SURFACE)]

B_INBOUND = [*B_SURFACE, *B_SUBWAY_INBOUND]

B_OUTBOUND = [*B_SUBWAY_OUTBOUND, *reversed(B_SURFACE)]

BONUS_ANNOUNCEMENTS = [
    *(
        "This train is being taken out of service. Please pardon any inconvenience we may be causing."
        for _ in range(5)
    ),
    "This train is running express to: Cleveland Circle.",
]

ALL_ANNOUNCEMENTS = [
    *LineRun(E_OUTBOUND),
    *LineRun(E_INBOUND),
    *LineRun(D_OUTBOUND),
    *LineRun(D_INBOUND),
    *LineRun(C_OUTBOUND),
    *LineRun(C_INBOUND),
    *LineRun(B_OUTBOUND),
    *LineRun(B_INBOUND),
    *BONUS_ANNOUNCEMENTS,
]


def main():
    for announce in ALL_ANNOUNCEMENTS:
        print(announce)


if __name__ == "__main__":
    main()
