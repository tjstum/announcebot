# Announce Bot
Infrastructure to generate known recorded announcement for various
subway systems

And (soon!) Twitter bots to keep the world thinking about them

## MBTA Green Line
Implemented in `greenline.py`, this contains the recorded
announcements for the four green line branches.

This was inspired by an old PHP script I wrote that generated the same
announcements. I used these when I was at MIT as my
[Zephyr](https://sipb.mit.edu/doc/zephyr/) signatures. I seem to have
lost the original PHP script, but it was structured quite
differently. There wasn't general organization of lines and platforms:
everything was station-oriented. I remember there being a giant list
with the various stations and a bunch of bools for which lines (B, C,
D or E) stopped there (in order to play the destinations). While there
was support for saying which side the doors opened on, it only
differentiated between inbound and outbound, so it didn't handle
stations like Park Street or Kenmore (with four used tracks)
particularly well. I think I used "duplicate" stations (with differing
amounts of spaces) to handle Park Street outbound.

Miscellaneous notes and decisions:
- When I lived in Boston, some B line inbound trains terminated at
  Park Street, but usually only during rush hour. Others continued to
  Government Center. It seems that the norm is termination at Park
  Street and has been for a while.
- This repository does not participate in the closure of E line north
  of North Station, so trains continue to run to Lechmere.
- I was only ever on an "express" (what we call "skip stop" in NYC)
  once, and it was on a C that skipped several street-level stops and
  ran express to Cleveland Circle
